# HuNI Solr docker image

This image contains the schema and other config files to run the HuNI Solr
server.

Note that the XSL transforms have been moved to the huni-harvester repo.

  https://bitbucket.org/huniteam/huni-harvester

To build and push the huniteam/huni-solr:5.5.4 docker image, create (or update)
the tag "bitbucket-pipeline" and push it to bitbucket.
