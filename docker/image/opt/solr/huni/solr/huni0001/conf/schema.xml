<?xml version="1.0" encoding="UTF-8" ?>
<!--
 Licensed to the Apache Software Foundation (ASF) under one or more
 contributor license agreements.  See the NOTICE file distributed with
 this work for additional information regarding copyright ownership.
 The ASF licenses this file to You under the Apache License, Version 2.0
 (the "License"); you may not use this file except in compliance with
 the License.  You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->

<!--
 This is the Solr schema file. This file should be named "schema.xml" and
 should be in the conf directory under the solr home
 (i.e. ./solr/conf/schema.xml by default)
 or located where the classloader for the Solr webapp can find it.

 This example schema is the recommended starting point for users.
 It should be kept correct and concise, usable out-of-the-box.

 For more information, on how to customize this file, please see
 http://wiki.apache.org/solr/SchemaXml

 PERFORMANCE NOTE: this schema includes many optional features and should not
 be used for benchmarking.  To improve performance one could
  - set stored="false" for all fields possible (esp large fields) when you
    only need to search on the field but don't need to return the original
    value.
  - set indexed="false" if you don't need to search on the field, but only
    return the field as a result of searching on other indexed fields.
  - remove all unneeded copyField statements
  - for best index size and searching performance, set "index" to false
    for all general text fields, use copyField to copy them to the
    catchall "text" field, and use that for searching.
  - For maximum indexing performance, use the StreamingUpdateSolrServer
    java client.
  - Remember to run the JVM in server mode, and use a higher logging level
    that avoids logging every request
-->

<schema name="example" version="1.5">
  <!-- attribute "name" is the name of this schema and is only used for display purposes.
       version="x.y" is Solr's version number for the schema syntax and
       semantics.  It should not normally be changed by applications.

       1.0: multiValued attribute did not exist, all fields are multiValued
            by nature
       1.1: multiValued attribute introduced, false by default
       1.2: omitTermFreqAndPositions attribute introduced, true by default
            except for text fields.
       1.3: removed optional field compress feature
       1.4: autoGeneratePhraseQueries attribute introduced to drive QueryParser
            behavior when a single string produces multiple tokens.  Defaults
            to off for version >= 1.4
       1.5: omitNorms defaults to true for primitive field types
            (int, float, boolean, string...)
     -->

 <fields>
   <!-- Valid attributes for fields:
     name: mandatory - the name for the field
     type: mandatory - the name of a field type from the
       <types> fieldType section
     indexed: true if this field should be indexed (searchable or sortable)
     stored: true if this field should be retrievable
     docValues: true if this field should have doc values. Doc values are
       useful for faceting, grouping, sorting and function queries. Although not
       required, doc values will make the index faster to load, more
       NRT-friendly and more memory-efficient. They however come with some
       limitations: they are currently only supported by StrField, UUIDField
       and all Trie*Fields, and depending on the field type, they might
       require the field to be single-valued, be required or have a default
       value (check the documentation of the field type you're interested in
       for more information)
     multiValued: true if this field may contain multiple values per document
     omitNorms: (expert) set to true to omit the norms associated with
       this field (this disables length normalization and index-time
       boosting for the field, and saves some memory).  Only full-text
       fields or fields that need an index-time boost need norms.
       Norms are omitted for primitive (non-analyzed) types by default.
     termVectors: [false] set to true to store the term vector for a
       given field.
       When using MoreLikeThis, fields used for similarity should be
       stored for best performance.
     termPositions: Store position information with the term vector.
       This will increase storage costs.
     termOffsets: Store offset information with the term vector. This
       will increase storage costs.
     required: The field is required.  It will throw an error if the
       value does not exist
     default: a value that should be used if no value is specified
       when adding a document.
   -->

   <!-- field names should consist of alphanumeric or underscore characters only and
      not start with a digit.  This is not currently strictly enforced,
      but other field names will not have first class support from all components
      and back compatibility is not guaranteed.  Names with both leading and
      trailing underscores (e.g. _version_) are reserved.
   -->

   <!-- Note that biography and description are set indexed="false", but they
        are still added to the text and text_rev fields. This means they still
        appear in search results, but no longer can be searched by exact
        string match. This doesn't really make sense for long fields anyway.
        This is because some entries broke the 32k char limit -->


    <!-- DO NOT REMOVE lest you want Solr to crack it at you -->
    <field name="_version_" type="long" indexed="true" stored="true"/>

    <!-- the field used as the unique id for the document -->
    <field name="docid"               type="string" required="true" multiValued="false" />
    <field name="document_history"    type="string" required="true"  multiValued="true" stored="true" />
    <field name="provider_source"     type="string" required="true"  multiValued="false" stored="true" />

    <!-- Deleted records get this flag -->
    <field name="isDeleted" type="boolean" stored="true" />

    <!-- string fields -->
    <field type="string" multiValued="false" indexed="true" stored="true" name="entityType" />

    <field type="string" multiValued="false" indexed="false" stored="true" name="alternativeTitle" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="associatedWithOccupation" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="biography" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="contactDetails" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="country" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="creator" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="culturalHeritage" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="description" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="ethnicity" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="familyName" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="format" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="formatDetails" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="function" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="gender" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="individualName" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="isbn" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="issn" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="language" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="locality" />
    <field type="string" multiValued="true"  indexed="false" stored="true" name="mentions" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="name" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="occupation" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="postcode" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="primaryName" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="primaryTitle" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="publishedIn" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="state" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="subTitle" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="title" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="type" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="url" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="warning" />
    <field type="string" multiValued="false" indexed="false" stored="true" name="workCount" />

    <!-- date fields -->
    <field type="tdate" multiValued="false" indexed="false" stored="true" name="birthDate" />
    <field type="tdate" multiValued="false" indexed="false" stored="true" name="creationDate" />
    <field type="tdate" multiValued="false" indexed="false" stored="true" name="deathDate" />

    <field type="tdate" multiValued="false" indexed="true" stored="true" name="endDate" />
    <field type="tdate" multiValued="false" indexed="true" stored="true" name="releaseDate" />
    <field type="tdate" multiValued="false" indexed="true" stored="true" name="startDate" />

    <!-- provenance fields are not indexed-->
    <field type="string" multiValued="false" stored="true" name="sourceAgencyCode" />    <!-- prov_site_short -->
    <field type="string" multiValued="false" stored="true" name="sourceAgencyName" />    <!-- prov_site_long -->
    <field type="string" multiValued="false" stored="true" name="sourceID" />
    <field type="string" multiValued="false" stored="true" name="sourceRecordLink" />    <!-- prov_source -->
    <field type="string" multiValued="false" stored="true" name="sourceSiteAddress" />   <!-- prov_site_address -->
    <field type="string" multiValued="false" stored="true" name="sourceSiteTag" />       <!-- prov_site_tag -->
    <field type="tdate"  multiValued="false" stored="true" name="sourceLastUpdate" />     <!-- prov_doc_last_update -->

    <!-- catchall field, containing all other searchable text fields (implemented via copyField further on in this schema)  -->
    <field name="text" type="text_general" indexed="true" stored="false" multiValued="true"/>

    <!-- catchall text field that indexes tokens both normally and in reverse for efficient leading wildcard queries. -->
<!-- don't need it!
    <field name="text_rev" type="text_general_rev" indexed="true" stored="false" multiValued="true"/>
-->

 </fields>

    <!-- Field to use to determine and enforce document uniqueness.
      Unless this field is marked with required="false", it will be a required field
    -->
    <uniqueKey>docid</uniqueKey>

    <!-- copyField commands copy one field to another at the time a document
        is added to the index.  It's used either to index the same field differently,
        or to add multiple fields to the same field for easier/faster searching.
    -->

    <copyField dest="text" source="alternativeTitle" />
    <copyField dest="text" source="associatedWithOccupation" />
    <copyField dest="text" source="biography" />
    <copyField dest="text" source="contactDetails" />
    <copyField dest="text" source="country" />
    <copyField dest="text" source="creator" />
    <copyField dest="text" source="culturalHeritage" />
    <copyField dest="text" source="description" />
    <copyField dest="text" source="ethnicity" />
    <copyField dest="text" source="familyName" />
    <copyField dest="text" source="format" />
    <copyField dest="text" source="formatDetails" />
    <copyField dest="text" source="function" />
    <copyField dest="text" source="gender" />
    <copyField dest="text" source="individualName" />
    <copyField dest="text" source="isbn" />
    <copyField dest="text" source="issn" />
    <copyField dest="text" source="language" />
    <copyField dest="text" source="locality" />
    <copyField dest="text" source="mentions" />
    <copyField dest="text" source="name" />
    <copyField dest="text" source="occupation" />
    <copyField dest="text" source="postcode" />
    <copyField dest="text" source="publishedIn" />
    <copyField dest="text" source="state" />
    <copyField dest="text" source="subTitle" />
    <copyField dest="text" source="title" />
    <copyField dest="text" source="type" />

    <!-- map the various date formats onto startDate and endDate for easier range searching -->
    <copyField dest="startDate" source="birthDate" />
    <copyField dest="startDate" source="creationDate" />
    <copyField dest="endDate"   source="deathDate" />

  <types>
    <!-- field type definitions. The "name" attribute is
       just a label to be used by field definitions.  The "class"
       attribute and any other attributes determine the real
       behavior of the fieldType.
         Class names starting with "solr" refer to java classes in a
       standard package such as org.apache.solr.analysis
    -->

    <!-- The StrField type is not analyzed, but indexed/stored verbatim.
       It supports doc values but in that case the field needs to be
       single-valued and either required or have a default value.
      -->
    <fieldType name="string" class="solr.StrField" sortMissingLast="true" />

    <!-- boolean type: "true" or "false" -->
    <fieldType name="boolean" class="solr.BoolField" sortMissingLast="true"/>

    <!-- sortMissingLast and sortMissingFirst attributes are optional attributes are
         currently supported on types that are sorted internally as strings
         and on numeric types.
	     This includes "string","boolean", and, as of 3.5 (and 4.x),
	     int, float, long, date, double, including the "Trie" variants.
       - If sortMissingLast="true", then a sort on this field will cause documents
         without the field to come after documents with the field,
         regardless of the requested sort order (asc or desc).
       - If sortMissingFirst="true", then a sort on this field will cause documents
         without the field to come before documents with the field,
         regardless of the requested sort order.
       - If sortMissingLast="false" and sortMissingFirst="false" (the default),
         then default lucene sorting will be used which places docs without the
         field first in an ascending sort and last in a descending sort.
    -->

    <!--
      Default numeric field types. For faster range queries, consider the tint/tfloat/tlong/tdouble types.

      These fields support doc values, but they require the field to be
      single-valued and either be required or have a default value.
    -->
    <fieldType name="int" class="solr.TrieIntField" precisionStep="0" positionIncrementGap="0"/>
    <fieldType name="float" class="solr.TrieFloatField" precisionStep="0" positionIncrementGap="0"/>
    <fieldType name="long" class="solr.TrieLongField" precisionStep="0" positionIncrementGap="0"/>
    <fieldType name="double" class="solr.TrieDoubleField" precisionStep="0" positionIncrementGap="0"/>

    <!--
     Numeric field types that index each value at various levels of precision
     to accelerate range queries when the number of values between the range
     endpoints is large. See the javadoc for NumericRangeQuery for internal
     implementation details.

     Smaller precisionStep values (specified in bits) will lead to more tokens
     indexed per value, slightly larger index size, and faster range queries.
     A precisionStep of 0 disables indexing at different precision levels.
    -->
    <fieldType name="tint" class="solr.TrieIntField" precisionStep="8" positionIncrementGap="0"/>
    <fieldType name="tfloat" class="solr.TrieFloatField" precisionStep="8" positionIncrementGap="0"/>
    <fieldType name="tlong" class="solr.TrieLongField" precisionStep="8" positionIncrementGap="0"/>
    <fieldType name="tdouble" class="solr.TrieDoubleField" precisionStep="8" positionIncrementGap="0"/>

    <!-- The format for this date field is of the form 1995-12-31T23:59:59Z, and
         is a more restricted form of the canonical representation of dateTime
         http://www.w3.org/TR/xmlschema-2/#dateTime
         The trailing "Z" designates UTC time and is mandatory.
         Optional fractional seconds are allowed: 1995-12-31T23:59:59.999Z
         All other components are mandatory.

         Expressions can also be used to denote calculations that should be
         performed relative to "NOW" to determine the value, ie...

               NOW/HOUR
                  ... Round to the start of the current hour
               NOW-1DAY
                  ... Exactly 1 day prior to now
               NOW/DAY+6MONTHS+3DAYS
                  ... 6 months and 3 days in the future from the start of
                      the current day

         Consult the DateField javadocs for more information.

         Note: For faster range queries, consider the tdate type
      -->
    <fieldType name="date" class="solr.TrieDateField" precisionStep="0" positionIncrementGap="0"/>

    <!-- A Trie based date field for faster date range queries and date faceting. -->
    <fieldType name="tdate" class="solr.TrieDateField" precisionStep="6" positionIncrementGap="0"/>

    <!-- A general text field that has reasonable, generic
         cross-language defaults: it tokenizes with StandardTokenizer,
	 removes stop words from case-insensitive "stopwords.txt"
	 (empty by default), and down cases.  At query time only, it
	 also applies synonyms. -->
    <fieldType name="text_general" class="solr.TextField" positionIncrementGap="100">
      <analyzer type="index">
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.StopFilterFactory" ignoreCase="true" words="stopwords.txt" enablePositionIncrements="true" />
        <!-- in this example, we will only use synonyms at query time
        <filter class="solr.SynonymFilterFactory" synonyms="index_synonyms.txt" ignoreCase="true" expand="false"/>
        -->
        <filter class="solr.LowerCaseFilterFactory"/>
      </analyzer>
      <analyzer type="query">
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.StopFilterFactory" ignoreCase="true" words="stopwords.txt" enablePositionIncrements="true" />
        <filter class="solr.SynonymFilterFactory" synonyms="synonyms.txt" ignoreCase="true" expand="true"/>
        <filter class="solr.LowerCaseFilterFactory"/>
      </analyzer>
    </fieldType>

    <!-- A text field with defaults appropriate for English, plus
	 aggressive word-splitting and autophrase features enabled.
	 This field is just like text_en, except it adds
	 WordDelimiterFilter to enable splitting and matching of
	 words on case-change, alpha numeric boundaries, and
	 non-alphanumeric chars.  This means certain compound word
	 cases will work, for example query "wi fi" will match
	 document "WiFi" or "wi-fi".
        -->

    <!-- since fields of this type are by default not stored or indexed,
         any data added to them will be ignored outright.  -->
    <fieldtype name="ignored" stored="false" indexed="false" multiValued="true" class="solr.StrField" />

 </types>

</schema>
