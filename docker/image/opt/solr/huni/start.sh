#!/bin/sh

# We set the maximum and minimum memory sizes to avoid the cost of scaling
# the memory.
: ${SOLR_RAM:=3g}

echo Starting Solr with $SOLR_RAM RAM.
exec java -Xmx$SOLR_RAM -Xms$SOLR_RAM -jar start.jar --module=http
