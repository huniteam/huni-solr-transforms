#!/bin/bash

IMAGE=huniteam/huni-solr:5.5.4

case $1 in

    build)
        docker build -t $IMAGE .
        ;;

    push)
        docker push $IMAGE
        ;;

    bash)
        docker run --rm -ti $IMAGE bash
        ;;

    *)
        echo "usage: $0 [build|push]"
        exit 1

        ;;

esac

